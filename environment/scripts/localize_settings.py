#!/usr/bin/python3

import grp
import os
import pwd
import sys

if len(sys.argv) > 1:
    # handles ansible_local
    docroot = sys.argv[1]
else:
    fd = os.path.dirname(os.path.realpath(__file__))
    docroot = os.path.abspath(os.path.join(fd, "..", "..", "docroot"))

settings_php = os.path.join(docroot, "sites/default/settings.php")
settings_local_php = os.path.join(docroot, "sites/default/settings.local.php")

# read settings.php
try:
    fh = open(settings_php, "r")
except IOError as e:
    print("unable to read {0}: {1}".format(settings_php, e.strerror))
    sys.exit(1)

lines = [l.strip("\n") for l in fh.readlines()]
fh.close()

settings_local = ["<?php"]
stop_at = len(lines)
for i in range(len(lines)):
    line = lines[i]

    # grab hash salt
    if line.startswith("$settings['hash_salt']"):
        settings_local.append(line)
        lines[i] = "$settings['hash_salt'] = '';"
    elif line.startswith("$databases['default']['default'] = array ("):
        stop_at = i
        settings_local.append(line)
    elif i >= stop_at:
        settings_local.append(line)

try:
    fh = open(settings_php, "w")
except IOError as e:
    print("unable to write {0}: {1}".format(settings_php, e.strerror))
    sys.exit(1)

# truncate settings.php
for i in range(stop_at):
    print(lines[i], file=fh)
fh.close()

try:
    fh = open(settings_local_php, "w")
except IOError as e:
    print("unable to write {0}: {1}".format(settings_local_php, e.strerror))
    sys.exit(1)

# write out local settings to settings.local.php
for line in settings_local:
    print(line, file=fh)
fh.close()

# chown settings{.local}.php back to www-data
uid = pwd.getpwnam("www-data").pw_uid
gid = grp.getgrnam("www-data").gr_gid
os.chown(settings_php, uid, gid)
os.chown(settings_local_php, uid, gid)
