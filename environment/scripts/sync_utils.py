import os
import shutil
import subprocess
import sys

ABORT=0
PROMPT=1
OVERWRITE=2

class Syncer(object):
    def __init__(self, src_dir, conflict, dest_dir, timeout, debug):
        if os.path.isdir(src_dir) and os.access(src_dir, os.R_OK | os.X_OK):
            self._src_dir = src_dir
        else:
            raise SyncerException("{0} not a valid directory or insufficient access".format(src_dir))

        if os.path.isdir(dest_dir) and os.access(dest_dir, os.R_OK | os.W_OK | os.X_OK):
            self._dest_dir = dest_dir
        else:
            raise SyncerException("{0} not a valid directory or insufficient access".format(dest_dir))

        self._conflict = conflict
        self._timeout = timeout
        self._debug = debug

        self._git = subprocess.check_output(["which", "git"], universal_newlines=True).strip()
        self._rsync = subprocess.check_output(["which", "rsync"], universal_newlines=True).strip()

        # get owner of dest_dir
        stat = os.stat(self._dest_dir)
        self._dest_uid = stat.st_uid
        self._dest_gid = stat.st_gid

        self._wd = self._src_dir

    def _print_debug(self, cmd, stdout, stderr):
        if type(cmd) == list:
            print("command: " + " ".join(cmd), file=sys.stderr)
        else:
            print("command: {0}".format(cmd), file=sys.stderr)
        if stdout:
            print("stdout: {0}".format(stdout), file=sys.stderr)
        if stderr:
            print("stderr: {0}".format(stderr), file=sys.stderr)

    def _err_exit(self, msg, code=1):
        print(msg, file=sys.stderr)
        sys.exit(code)

    def _call_process(self, cmd, err_message):
        process = subprocess.Popen(cmd,
            stdout=subprocess.PIPE, stderr=subprocess.PIPE,
            universal_newlines=True, cwd=self._wd)

        try:
            stdout, stderr = process.communicate(timeout=self._timeout)
        except subprocess.CalledProcessError as e:
            self._print_debug(cmd, None, None)
            self._err_exit("{0}: {1}".format(err_message, e.stderr))
        except subprocess.TimeoutExpired:
            self._print_debug(cmd, None, None)
            self._err_exit("{0}: timed out".format(err_message))
        except:
            self._print_debug(cmd, None, None)
            self._err_exit("{0}: an error occurred".format(err_message))

        # debug output
        if self._debug:
            self._print_debug(cmd, stdout, stderr)
            print("", file=sys.stderr)

        return stdout

    # helper routines

    def _get_file_lists(self):
        # get definitive list of files for src
        self._wd = self._src_dir
        # get a list of all files
        src_files = set()
        for dirpath, dirnames, filenames in os.walk(self._src_dir):
            if not dirpath.startswith(os.path.join(self._src_dir, ".git")):
                for filename in filenames:
                    src_files.add(os.path.join(dirpath, filename))
        # exclude the files which git is ignoring
        cmd = [self.git, "ls-files", "-i", "-X", ".gitignore", "-X",
            ".git/info/exclude", "--others"]
        stdout = self._call_process(cmd, "failed to find excludes")
        do_exclude = set([os.path.join(self._src_dir, l) for l in stdout.splitlines()])
        # cut out ignored files
        src_files.difference_update(do_exclude)

        # get a list of staged changes
        cmd = [self.git, "diff", "--cached", "--name-only"]
        stdout = self._call_process(cmd, "failed to find staged changes")
        staged_changes = set([os.path.join(self._src_dir, l) for l in stdout.splitlines()])
        # gives added AND removed files without distinction
        # add additional check here to see if the file is in src_files
        self._added_files = staged_changes.intersection(src_files)
        self._removed_files = staged_changes.difference(src_files)

        # get a list of unstaged changes
        cmd = [self.git, "ls-files", "-d", "-m", "--others"]
        stdout = self._call_process(cmd, "failed to find unstaged changes")
        unstaged_changes = set([os.path.join(self._src_dir, l) for l in stdout.splitlines()])
        # cut out the ignored files
        unstaged_changes.difference_update(do_exclude)

        self._added_files.update(unstaged_changes.intersection(src_files))
        self._removed_files.update(unstaged_changes.difference(src_files))

    def _get_current_branch(self, wd):
        self._wd = wd
        # run command
        cmd = [self.git, "branch"]
        stdout = self._call_process(cmd, "failed to get branches")

        # process output
        branches = stdout.splitlines()
        return [b.strip("* ") for b in branches if b.startswith("* ")][0]

    # worker routines

    def _reset_changes(self):
        """
        Resets all changes (including untracked files)
        """
        self._wd = self._dest_dir

        if self._conflict < OVERWRITE:
            dest_files = set()
            for dirpath, dirnames, filenames in os.walk(self._dest_dir):
                if not dirpath.startswith(os.path.join(self._dest_dir, ".git")):
                    for filename in filenames:
                        dest_files.add(os.path.join(dirpath, filename))
            # exclude the files which git is ignoring
            cmd = [self.git, "ls-files", "-i", "-X", ".gitignore", "-X",
                ".git/info/exclude", "--others"]
            stdout = self._call_process(cmd, "failed to find excludes")
            do_exclude = set([os.path.join(self._dest_dir, l) for l in stdout.splitlines()])
            # cut out ignored files
            dest_files.difference_update(do_exclude)

            # get a list of staged changes
            cmd = [self.git, "diff", "--cached", "--name-only"]
            stdout = self._call_process(cmd, "failed to find staged changes")
            staged_changes = set([os.path.join(self._dest_dir, l) for l in stdout.splitlines()])
            # gives added AND removed files without distinction
            # add additional check here to see if the file is in dest_files
            added_files = staged_changes.intersection(dest_files)
            removed_files = staged_changes.difference(dest_files)

            # get a list of unstaged changes
            cmd = [self.git, "ls-files", "-d", "-m", "--others"]
            stdout = self._call_process(cmd, "failed to find unstaged changes")
            unstaged_changes = set([os.path.join(self._dest_dir, l) for l in stdout.splitlines()])
            # cut out the ignored files
            unstaged_changes.difference_update(do_exclude)

            added_files.update(unstaged_changes.intersection(dest_files))
            removed_files.update(unstaged_changes.difference(dest_files))

            added_files = sorted(added_files)
            removed_files = sorted(removed_files)

            if len(added_files + removed_files) > 0:
                message = "This operation would add or modify the following files:\n " + "\n ".join(added_files)
                message += "\nIt would also remove the following files:\n " + "\n ".join(removed_files)
                if self._conflict == ABORT:
                    message += "\nAborting"
                    self._err_exit(message, 0)
                else:
                    print(message)
                    prompt = "Is this okay? [y|N] "
                    response = input(prompt).lower().strip()
                    while not (response.startswith('y') or response.startswith('n')):
                        prompt = "Is this okay? [y|N] "
                        response = input(prompt).lower().strip()
                    if response.startswith('n'):
                        self._err_exit("Aborting", 0)
                    # else falls out of the conditional into the code below

        # unstage any staged (but not committed) changes
        cmd = [self.git, "checkout", "."]
        self._call_process(cmd, "failed to checkout")

        # dump any changed files
        cmd = [self.git, "reset", "HEAD"]
        self._call_process(cmd, "failed to reset")

        # clean up untracked files
        cmd = [self.git, "clean", "-f", "-d"]
        self._call_process(cmd, "failed to clean")

    def _fetch(self):
        self._wd = self._dest_dir
        # run command
        cmd = [self.git, "fetch"]
        self._call_process(cmd, "failed to fetch")

    def _merge_upstream(self):
        self._wd = self._dest_dir

        # run command
        cmd = [self.git, "merge", "origin/{0}".format(self._src_branch)]
        self._call_process(cmd, "failed to merge")

    def _checkout_working_branch(self):
        """
        Check out a new working branch
        """
        self._wd = self._dest_dir
        # check if branch exists
        cmd = [self.git, "branch"]
        stdout = self._call_process(cmd, "failed to get branches")
        branches = stdout.splitlines()

        # actually the current branch; we will check out a new branch to work in
        previous_branch = [b for b in branches if b.startswith("* ")][0]
        branches.remove(previous_branch)
        previous_branch = previous_branch.strip("* ")
        branches = branches + [previous_branch]

        if self._src_branch in branches:
            cmd = [self.git, "checkout", self._src_branch]
        else:
            cmd = [self.git, "checkout", "-b", self._src_branch]

        # run command
        stdout = self._call_process(cmd, "failed to check out new branch")

    def _sync_branches(self):
        self._wd = self._src_dir

        # get current branch on src
        self._src_branch = self._get_current_branch(self._src_dir)
        # go to dest dir, stash, then fetch
        self._fetch()
        # get current branch on dest
        self._dest_branch = self._get_current_branch(self._dest_dir)

        self._reset_changes()

        if not self._dest_branch == self._src_branch:
            self._checkout_working_branch()

        self._merge_upstream()

    def do_sync(self):
        """
        Updates modules
        """
        self._wd = self._src_dir

        self._sync_branches()
        self._get_file_lists()

        added_files = sorted(self._added_files)
        removed_files = sorted(self._removed_files)

        if self._conflict == ABORT and len(added_files + removed_files) > 0:
            message = "This operation would add or modify the following files:\n " + "\n ".join(added_files)
            message += "\nIt would also remove the following files:\n " + "\n ".join(removed_files)
            message += "\nAborting"
            self._err_exit(message, 0)

        # remove files not in src_files
        for filename in removed_files:
            filename = filename.replace(self._src_dir, self._dest_dir)
            if self._conflict == PROMPT:
                print(filename + " will be REMOVED")
                prompt = "Is this okay? [y|N] "
                response = input(prompt).lower().strip()
                while not (response.startswith('y') or response.startswith('n')):
                    prompt = "Is this okay? [y|N] "
                    response = input(prompt).lower().strip()
                if response.startswith('n'):
                    continue
                # else falls out of the conditional into the code below
            os.remove(filename)

        # copy added/changed files over
        for src_file in added_files:
            dest_file = src_file.replace(self._src_dir, self._dest_dir)
            dest_dir = os.path.dirname(dest_file)
            if self._conflict == PROMPT:
                if os.path.isfile(dest_file):
                    message = dest_file + " will be OVERWRITTEN"
                else:
                    message = dest_file + " will be CREATED"
                print(message)
                prompt = "Is this okay? [y|N] "
                response = input(prompt).lower().strip()
                while not (response.startswith('y') or response.startswith('n')):
                    prompt = "Is this okay? [y|N] "
                    response = input(prompt).lower().strip()
                if response.startswith('n'):
                    continue
                # else falls out of the conditional into the code below
            if not os.path.isdir(dest_dir):
                os.makedirs(dest_dir)
            shutil.copy2(src_file, dest_file)

        # remove empty directories, chown back to owner of dest_dir
        for pathdir, dirnames, filenames in os.walk(self._dest_dir):
            if len(filenames) == 0 and len(dirnames) == 0:
                os.rmdir(pathdir)
                continue

            os.chown(pathdir, self._dest_uid, self._dest_gid)
            for dirname in dirnames:
                os.chown(os.path.join(pathdir, dirname), self._dest_uid, self._dest_gid)
            for filename in filenames:
                os.chown(os.path.join(pathdir, filename), self._dest_uid, self._dest_gid)

    @property
    def git(self):
        return self._git

class SyncerException(Exception):
    def __init__(self, message):
        super().__init__(message)
        self._message = message

    @property
    def message(self):
        return self._message
