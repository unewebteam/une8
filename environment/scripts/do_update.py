#!/usr/bin/python3

import datetime
import grp
import os
import pwd
import subprocess
import sys

class DrupalModule(object):
    def __init__(self, name, label, existing_version, candidate_version):
        self._name = name
        self._label = label
        self._existing_version = existing_version
        self._candidate_version = candidate_version

    @property
    def name(self):
        return self._name

    @property
    def label(self):
        return self._label

    @property
    def existing_version(self):
        return self._existing_version

    @property
    def candidate_version(self):
        return self._candidate_version

    @property
    def source_dir(self):
        return self._source_dir
    @source_dir.setter
    def source_dir(self, dirname):
        self._source_dir = dirname

class Updater(object):
    BEGIN    =  0
    STASH    =  1
    CHECKOUT =  2
    UPDATE   =  3
    STAGE    =  4
    COMMIT   =  5
    PUSH     =  6
    UNSTASH  =  7
    def __init__(self, repo_dir, timeout=5, debug=False):
        if os.path.isdir(repo_dir) and os.access(repo_dir, os.R_OK | os.W_OK | os.X_OK):
            self._repo_dir = repo_dir
        else:
            raise UpdaterException("{0} not a valid directory or insufficient access".format(repo_dir))

        self._timeout = timeout
        self._debug = debug

        self._docroot = os.path.join(self._repo_dir, "docroot")
        self._drush = os.path.join(self._repo_dir, "bin/drush")
        self._git = subprocess.check_output(["which", "git"], universal_newlines=True).strip()
        self._wd = self._docroot

        # get owner of repo_dir
        stat = os.stat(self._repo_dir)
        self._web_uid = stat.st_uid
        self._web_gid = stat.st_gid

        # set environment
        web_user  = pwd.getpwuid(self._web_uid).pw_name
        self._env             = os.environ.copy()
        self._env["HOME"]     = pwd.getpwnam(web_user).pw_dir
        self._env["LOGNAME"]  = web_user
        self._env["MAIL"]     = "/var/mail/{0}".format(web_user)
        self._env["USER"]     = web_user
        self._env["USERNAME"] = web_user

        self._set_update_candidates()
        self._updated = []

        self._unraveling = False
        self._state = self.BEGIN

    def _print_debug(self, cmd, stdout, stderr):
        if type(cmd) == list:
            print("command: " + " ".join(cmd), file=sys.stderr)
        else:
            print("command: {0}".format(cmd), file=sys.stderr)
        if stdout:
            print("stdout: {0}".format(stdout), file=sys.stderr)
        if stderr:
            print("stderr: {0}".format(stderr), file=sys.stderr)

    def _err_exit(self, msg, code=1):
        print(msg, file=sys.stderr)
        sys.exit(code)

    def _demote(self):
        os.setgid(self._web_gid)
        os.setuid(self._web_uid)

    def _call_process(self, cmd, err_message):
        if self._as_user:
            process = subprocess.Popen(cmd,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                universal_newlines=True, cwd=self._wd, env=self._env,
                preexec_fn=self._demote())
        else:
            process = subprocess.Popen(cmd,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                universal_newlines=True, cwd=self._wd)

        try:
            stdout, stderr = process.communicate(timeout=self._timeout)
        except subprocess.CalledProcessError as e:
            self._print_debug(cmd, None, None)
            if not self._unraveling:
                self._unravel()
            self._err_exit("{0}: {1}".format(err_message, e.stderr))
        except subprocess.TimeoutExpired:
            self._print_debug(cmd, None, None)
            if not self._unraveling:
                self._unravel()
            self._err_exit("{0}: timed out".format(err_message))
        except:
            self._print_debug(cmd, None, None)
            if not self._unraveling:
                self._unravel()
            self._err_exit("{0}: an error occurred".format(err_message))

        # debug output
        if self._debug:
            self._print_debug(cmd, stdout, stderr)
            print("", file=sys.stderr)

        return stdout

    def _unravel(self):
        """
        Attempts to return repo to previous state after failure
        """
        self._unraveling = True
        self._debug = True
        err_message = "failed to unravel"

        if self._state >= self.PUSH:
            self._err_exit("not unraveling; locally stashed changes may be unapplied")

        if self._state == self.COMMIT:
            self._as_user = True
            cmd = [self.git, "reset", "HEAD~"]
            self._call_process(cmd, "{0} commit".format(err_message))

            self._state = self._state - 1

        for module in self._updated:
            self._as_user = True
            self._wd = module.source_dir

            # unravel add
            if self._state == self.STAGE:
                cmd = [self.git, "reset"]
                self._call_process(cmd, "{0} stage".format(err_message))

            # unravel update
            if self._state >= self.UPDATE:
                cmd = [self.git, "clean", "-f", "-d"]
                self._call_process(cmd, "{0} update".format(err_message))
                cmd = [self.git, "checkout", "--", "."]
                self._call_process(cmd, "{0} update".format(err_message))

        self._state = self.CHECKOUT

        if self._state == self.CHECKOUT:
            cmd = [self.git, "checkout", "-"]
            self._call_process(cmd, "{0} checkout".format(err_message))
            self._state = self._state - 1

        if self._state == self.STASH:
            cmd = [self.git, "stash", "--apply"]
            self._call_process(cmd, "{0} stash".format(err_message))
            self._state = self._state - 1

    def _set_update_candidates(self):
        """
        Sets candidates for updating
        """
        self._as_user = True
        # run command
        cmd = [self.drush, "pm-updatestatus",
               "--fields=name,label,existing_version,candidate_version",
               "--format=csv"]
        stdout = self._call_process(cmd, "failed to get update candidates")

        # process output
        self._candidates = [DrupalModule(*c.split(",")) for c in stdout.splitlines()]
        for mod in self._candidates:
            cmd = [self.drush, "pm-info", "--fields=path", mod.name, "--format=csv"]
            stdout = self._call_process(cmd, "failed to get module directory")

            mod.source_dir = os.path.join(self.docroot, stdout.strip())

    def _stash_changes(self):
        """
        Stashes uncommitted changes (including untracked files)
        """
        self._as_user = True
        # run command
        cmd = [self.git, "stash", "save", "-u"]
        stdout = self._call_process(cmd, "failed to stash changes")

        # process output
        self._stashed_changes = (stdout.strip() != "No local changes to save")


    def _checkout_working_branch(self):
        """
        Check out a new working branch
        """
        self._as_user = True
        today = datetime.date.today()
        self._branch_name = "update/{0}{1:02d}{2:02d}".format(today.year,
            today.month, today.day)

        # check if branch exists
        cmd = [self.git, "branch"]
        stdout = self._call_process(cmd, "failed to get branches")
        branches = stdout.splitlines()

        # actually the current branch; we will check out a new branch to work in
        previous_branch = [b for b in branches if b.startswith("* ")][0]
        branches.remove(previous_branch)
        previous_branch = previous_branch.strip("* ")
        self._branches = branches + [previous_branch]

        if self.branch_name in self._branches:
            cmd = [self.git, "checkout", self.branch_name]
        else:
            cmd = [self.git, "checkout", "-b", self.branch_name]

        # run command
        self._state = self.CHECKOUT
        stdout = self._call_process(cmd, "failed to check out new branch")

        self._branches.append(self.branch_name)

    def _update_module(self, module):
        """
        Update a given module using drush
        """
        self._as_user = True
        self._wd = module.source_dir
        self._updated.append(module)

        # run command
        self._state = self.UPDATE
        cmd = [self.drush, "up", module.name, "-y"]
        self._call_process(cmd, "failed to update module {0}".format(module.name))

    def _stage_changes(self, module):
        """
        Stage changes (git add)
        """
        self._as_user = True
        self._wd = module.source_dir

        # run command
        self._state = self.STAGE
        cmd = [self.git, "add", "."]
        self._call_process(cmd, "failed to add changes for {0}".format(module.name))

    def _commit_changes(self, module):
        """
        Commit changes to repo
        """
        self._as_user = True
        self._wd = module.source_dir
        message = "{0} updated from {1} to {2}".format(module.label,
            module.existing_version, module.candidate_version)

        # run command
        self._state = self.COMMIT
        cmd = [self.git, "commit", "-m", message]
        self._call_process(cmd, "failed to commit changes for {0}".format(module.name))

    def _push_changes(self):
        """
        Push changes to remote
        """
        self._as_user = False
        self._wd = self.docroot

        # check if remote branch exists
        cmd = [self.git, "branch", "-r"]
        stdout = self._call_process(cmd, "failed to get remote branches")
        branches = [b.strip() for b in stdout.splitlines()]

        cmd = [self.git, "push", "-u", "origin", self.branch_name]
        for branch in branches:
            # first check if origin's HEAD is pointing to our current branch
            if branch.startswith("origin/HEAD"):
                head = branch.split(" -> ")[1]
                # abort
                if head == "origin/{0}".format(self.branch_name):
                    self._err_exit("origin/HEAD points to this branch! fix and push manually")
            elif branch == "origin/{0}".format(self.branch_name):
                cmd = [self.git, "push", "origin", self.branch_name]

        # run command
        self._state = self.PUSH
        self._call_process(cmd, "failed to push changes")

    def _unstash_changes(self):
        """
        Reapply local changes to repo
        """
        # run command
        cmd = [self.git, "stash", "apply"]
        self._call_process(cmd, "failed to unstash local changes")

    def update(self):
        """
        Updates modules
        """
        if not self._candidates:
            self._err_exit("Nothing to update", code=0)

        self._wd = self.docroot

        # stash changes
        self._stash_changes()
        # check out new branch
        self._checkout_working_branch()
        # update modules one at a time
        for module in self.candidates:
            # update code
            self._update_module(module)
            # stage changes in git
            self._stage_changes(module)
            # commit changes to git
            self._commit_changes(module)
        # push changes
        self._push_changes()
        # re-apply local changes
        if self._stashed_changes:
            self._unstash_changes()

    @property
    def docroot(self):
        return self._docroot

    @property
    def working_directory(self):
        return self._wd

    @property
    def drush(self):
        return self._drush

    @property
    def git(self):
        return self._git

    @property
    def branch_name(self):
        return self._branch_name

    @property
    def candidates(self):
        return self._candidates

class UpdaterException(Exception):
    def __init__(self, message):
        super().__init__(message)
        self._message = message

    @property
    def message(self):
        return self._message

# run the updater
repo_dir = "/var/www/une"
if "-t" in sys.argv:
    t_index = sys.argv.index("-t")
elif "--timeout" in sys.argv:
    t_index = sys.argv.index("--timeout")
else:
    t_index = -1

if t_index > -1:
    try:
        timeout = int(sys.argv[t_index+1])
    except:
        print("timeout requires an integer argument", file=sys.stderr)
        sys.exit(1)
else:
    timeout = 5

debug = ("-d" in sys.argv or "--debug" in sys.argv)
updater = Updater(repo_dir, timeout=timeout, debug=debug)

updater.update()
