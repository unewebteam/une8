#!/usr/bin/python3

import optparse
import sys

import sync_utils

parser = optparse.OptionParser(usage="Usage: %prog SRC DEST")
parser.add_option("-v", "--verbose", dest="verbose", action="store_true")
parser.add_option("-t", "--timeout", dest="timeout", action="store", type="int",
                  help="set timeout for subprocesses")
parser.add_option("-a", "--abort", dest="conflict", action="store_const", const=sync_utils.ABORT,
                  help="abort sync if conflict")
parser.add_option("-p", "--prompt", dest="conflict", action="store_const", const=sync_utils.PROMPT,
                  help="prompt for every conflicting file")
parser.add_option("-o", "--overwrite", dest="conflict", action="store_const", const=sync_utils.OVERWRITE,
                  help="overwrite files without asking")
parser.set_defaults(verbose=False, timeout=60, conflict=sync_utils.PROMPT)

(options, args) = parser.parse_args()

try:
    src_dir, dest_dir = args
except ValueError:
    parser.print_help()
    sys.exit(1)

syncer = sync_utils.Syncer(src_dir=src_dir, dest_dir=dest_dir, conflict=options.conflict, timeout=options.timeout, debug=options.verbose)
syncer.do_sync()
