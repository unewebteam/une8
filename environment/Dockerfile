#
# Dockerfile for unewebteam/une-drupal8
#

FROM php:7.1-apache

# testing deps
ENV TESTING_DEPS \
    git \
    libzip-dev \
    unzip \
    ssl-cert
RUN apt-get update && apt-get install -y \
    $TESTING_DEPS \
  --no-install-recommends && rm -r /var/lib/apt/lists/*

RUN docker-php-ext-install zip mbstring sockets

ENV APACHE_CONFDIR /etc/apache2

# Drupal requires mod_rewrite and mod_ssl
RUN a2enmod rewrite && a2enmod ssl

# disable default site and set up VirtualHosts
RUN a2dissite 000-default

RUN { \
    echo '<VirtualHost *:80>'; \
    echo '\tServerAdmin webmaster@localhost'; \
    echo '\tDocumentRoot /var/www/une8/docroot'; \
    echo '\tErrorLog ${APACHE_LOG_DIR}/error.log'; \
    echo '\tCustomLog ${APACHE_LOG_DIR}/access.log combined'; \
    echo '</VirtualHost>'; \
  } | tee "$APACHE_CONFDIR/sites-available/une8.conf" \
  && a2ensite une8

RUN { \
    echo '<IfModule mod_ssl.c>'; \
    echo '\t<VirtualHost _default_:443>'; \
    echo '\t\tServerAdmin webmaster@localhost'; \
    echo '\t\tDocumentRoot /var/www/une/docroot'; \
    echo '\t\tErrorLog ${APACHE_LOG_DIR}/error.log'; \
    echo '\t\tCustomLog ${APACHE_LOG_DIR}/access.log combined'; \
    echo '\t\tSSLEngine on'; \
        echo '\t\tSSLCertificateFile  /etc/ssl/certs/ssl-cert-snakeoil.pem'; \
    echo '\t\tSSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key'; \
    echo '\t\t<FilesMatch "\.(cgi|shtml|phtml|php)$">'; \
    echo '\t\t\tSSLOptions +StdEnvVars'; \
    echo '\t\t</FilesMatch>'; \
    echo '\t\t<Directory /usr/lib/cgi-bin>'; \
    echo '\t\t\tSSLOptions +StdEnvVars'; \
    echo '\t\t</Directory>'; \
    echo '\t</VirtualHost>'; \
    echo '</IfModule>'; \
  } | tee "$APACHE_CONFDIR/sites-available/une8_ssl.conf" \
  && a2ensite une8_ssl

# PHP files should be handled by PHP, and should be preferred over any other file type
RUN { \
    echo '<FilesMatch \.php$>'; \
    echo '\tSetHandler application/x-httpd-php'; \
    echo '</FilesMatch>'; \
    echo; \
    echo 'DirectoryIndex disabled'; \
    echo 'DirectoryIndex index.php index.html'; \
    echo; \
    echo '<Directory /var/www/>'; \
    echo '\tOptions -Indexes'; \
    echo '\tAllowOverride All'; \
    echo '</Directory>'; \
  } | tee "$APACHE_CONFDIR/conf-available/docker-php.conf" \
  && a2enconf docker-php

ENTRYPOINT ["docker-php-entrypoint"]

WORKDIR /var/www/html

EXPOSE 80 443
CMD ["apache2-foreground"]
