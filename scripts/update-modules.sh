#!/usr/bin/env bash

env_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../environment" && pwd )"

cd $env_dir
git gc >/dev/null 2>&1 # clean up repo
vagrant ssh -- "sudo /vagrant_data/environment/scripts/do_update.py"
