# University of New England Drupal 8 Site

## Development

### Requirements

1. git
    - macOS: command-line client via [Homebrew](https://brew.sh/) or [direct download](https://git-scm.com/download/mac) or pick your favorite [GUI client](https://git-scm.com/download/gui/mac)
    - Windows: command-line client via [direct download](https://git-scm.com/download/win) or pick your favorite [GUI client](https://git-scm.com/download/gui/windows)
    - Ubuntu: via `apt` (latest PPA [here](https://launchpad.net/~git-core/+archive/ubuntu/ppa))
    - or pick your favorite [GUI client](https://git-scm.com/download/gui/linux)
2. Vagrant
    - **SUPERSEDED**: this should be taken care of in the Vagrantfile itself
    - [download links](https://www.vagrantup.com/downloads.html)
    - vagrant-hostsupdater plugin ([installation instructions](https://github.com/cogitatio/vagrant-hostsupdater#installation))
    - vagrant-vbguest plugin ([installation instructions](https://github.com/dotless-de/vagrant-vbguest/#installation))

### Nice-to-have
1. drush
    - TODO: add a set of drush aliases for host machines
    - TODO: set up Acquia [drush integration](https://docs.acquia.com/acquia-cloud/drush/aliases)

### Setup

1. Clone this repository to wherever you keep web projects.
2. Navigate to the `environment` folder in a terminal.
3. Type `vagrant up`.
4. That's it! Once you're done, go to [une-local.dev/user](https://une-local.dev/user) in a browser. You should have gotten a message with your admin password once Vagrant successfully terminates.

### Workflow (provisional)

1. Fork this repo and clone it to your machine
2. `while not work.is_done`
    1. Check out a new git branch
    2. Make changes to code on your host OS
    3. Commit those changes to your new branch
    4. Run `scripts/sync-files.sh` to sync between host repo and guest instance (hacky, I know, but cross-platform)
    5. Test on une-local.dev (tests coming soon)
    6. After testing, push changes to your forks

### Warnings

- Don't use `update-modules.sh` just yet. It's still incomplete.
