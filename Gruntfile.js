module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // paths
        meta: {
            assetPath: 'docroot/themes/une/',
            assetPath_image: 'docroot/themes/une/images/',
        },
        // watch files
        watch: {
            css: {
                files: [
                    '<%= meta.assetPath %>/scss/**/*.scss'
                ],
                tasks: ['compass', 'bless']
            },
            images: {
                files: [
                    '<%= meta.assetPath_image %>/**/*.*'
                ],
                tasks: ['imagemin'],
                options: {
                    spawn: false,
                }
            }
        },
        // sass via compass
        compass: {
            dist: {
                options: {
                    sourcemap: true,
                    sassDir: '<%= meta.assetPath %>/scss/',
                    cssDir: '<%= meta.assetPath %>/css/',
                    environment: 'production',
                    outputStyle: 'expanded',
                    bundleExec: true
                }
            }
        },
        // blesscss
        bless: {
            css: {
                options: {
                    cacheBuster: false,
                    compress: true,
                    imports: true
                },
                files: {
                    '<%= meta.assetPath %>/css/iestyle.css': '<%= meta.assetPath %>/css/style.css'
                }
            }
        },
        // optimize images
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= meta.assetPath_image %>',
                        src: ['**/*.png'],
                        dest: '<%= meta.assetPath_image %>',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        expand: true,
                        cwd: '<%= meta.assetPath_image %>',
                        src: ['**/*.jpg'],
                        dest: '<%= meta.assetPath_image %>',
                        ext: '.jpg'
                    }
                ]
            }
        },
        // run browsersync
        browserSync: {
            dev: {
                bsFiles: {
                    src : '<%= meta.assetPath %>/css/*.css'
                },
                options: {
                    watchTask: true,
                    proxy: "<%= local.proxy %>"
                },
                ghostMode: {
                    clicks: true,
                    scroll: true,
                    links: true,
                    forms: true
                }
            }
        },
    });

    // plugins
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-bless');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-browser-sync');

    // tasks
    grunt.registerTask('crush', ['imagemin']);
    grunt.registerTask('browser', ['browserSync', 'watch']);
    grunt.registerTask('default', ['browser', 'crush']);
};
